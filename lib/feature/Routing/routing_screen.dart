import 'package:flutter/material.dart';
import 'package:metro/feature/widget/drawer_menu.dart';

class RoutingScreen extends StatelessWidget {
  const RoutingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('مسیریابی'),
      ),
      drawer: const DrawerMenu(),
      body: const Column(
        children: [],
      ),
    );
  }
}
