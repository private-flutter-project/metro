import 'package:flutter/material.dart';
import 'package:metro/core/lib/app_market/app_market.dart';
import 'package:metro/core/util/scroll_behavior.dart';
import 'package:share_plus/share_plus.dart';

import '../../core/util/routes.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [],
              ),
            ),
          ),
          Expanded(
            child: ScrollConfiguration(
              behavior: MyScrollBehavior(),
              child: ListView(
                padding: const EdgeInsets.all(0),
                children: [
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.menu,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'فهرست ایستگاه ها',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.stations);
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.navigation_rounded,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'ایستگاه ها در نقشه',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.stationsInMap);
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.navigation_rounded,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'مسیریابی',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.routing);
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.map,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'نقشه مترو',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.map);
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.settings,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'تنظیمات',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.settings);
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.share,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'اشتراک گذاری اپلیکیشن',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () async {
                      await Share.share('''
                      نصب برنامه مترو پلاس
                      
                      https://cafebazaar.ir/app/ir.skydevelopers.metro
                      '''
                          .trim());
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.apps,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'سایر محصولات ما',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      AppMarket.of(AppMarketType.cafeBazaar).showDevelopersApps(
                        developerId: 'skydevelopers',
                      );
                    },
                  ),
                  ListTile(
                    horizontalTitleGap: 4,
                    leading: const Icon(
                      Icons.info_outline,
                      size: 24,
                      color: Colors.black54,
                    ),
                    title: const Text(
                      'درباره ما',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff303031),
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.aboutUs);
                    },
                  ),
                ],
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'نسخه 1.0.0',
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
