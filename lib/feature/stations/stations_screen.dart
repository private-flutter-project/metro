import 'package:flutter/material.dart';
import 'package:metro/feature/widget/drawer_menu.dart';

class StationsScreen extends StatelessWidget {
  const StationsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ایستگاه ها'),
      ),
      drawer: const DrawerMenu(),
      body: const Column(
        children: [],
      ),
    );
  }
}
