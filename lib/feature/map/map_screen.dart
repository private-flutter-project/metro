import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../widget/drawer_menu.dart';

class MapScreen extends StatelessWidget {
  const MapScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'نقشه مترو',
        ),
      ),
      drawer: const DrawerMenu(),
      body: PhotoView(
        backgroundDecoration: const BoxDecoration(
          color: Colors.black,
        ),
        basePosition: Alignment.center,
        maxScale: 0.8,
        minScale: 0.07,
        initialScale: 0.3,
        imageProvider: const AssetImage(
          'assets/images/map/map.png',
        ),
      ),
    );
  }
}
