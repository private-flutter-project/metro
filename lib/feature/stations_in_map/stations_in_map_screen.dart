import 'package:flutter/material.dart';
import 'package:metro/feature/widget/drawer_menu.dart';

class StationsInMapScreen extends StatelessWidget {
  const StationsInMapScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ایستگاه ها در نقشه'),
      ),
      drawer: const DrawerMenu(),
      body: const Column(
        children: [],
      ),
    );
  }
}
