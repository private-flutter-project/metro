import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:metro/feature/map/map_screen.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'core/util/routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  blockLandscape();
  runApp(MyApp());
}

void blockLandscape() {
  // Block LandScape
  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
  );
  /////////////
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final route = Routes();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Metro',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('fa', 'IR'),
      ],
      navigatorObservers: [
        SentryNavigatorObserver(),
      ],
      locale: const Locale('fa', 'IR'),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "IranSans",
        scaffoldBackgroundColor: Colors.white,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.deepPurple,
        ),
        primaryColor: Colors.deepPurple,
        snackBarTheme: const SnackBarThemeData(
          contentTextStyle: TextStyle(
            fontFamily: "IranSans",
          ),
          backgroundColor: Colors.white,
          behavior: SnackBarBehavior.floating,
        ),
        appBarTheme: const AppBarTheme(
          titleTextStyle: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            fontFamily: "IranSans",
          ),
          centerTitle: true,
        ),
      ),
      onGenerateRoute: route.generateRoute,
      home: const MapScreen(),
    );
  }
}
