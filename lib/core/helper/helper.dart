import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Helper {
  static void log(
    String message, {
    bool isForce = false,
  }) {
    if (kIsWeb && !isForce && !kDebugMode) {
      return;
    }

    debugPrint(message);
  }

  static showSnackBar(
    String message,
    BuildContext context,
  ) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
        ),
      ),
    );
  }
}
