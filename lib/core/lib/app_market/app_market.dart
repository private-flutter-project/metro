import 'market/cafebazaar.dart';
import 'market/myket.dart';
import 'market/playStore.dart';

abstract class AppMarket {
  Future<void> showAppPage({
    String? packageName,
  });

  Future<void> showDevelopersApps({
    required String developerId,
  });

  factory AppMarket.of(AppMarketType type) {
    switch (type) {
      case AppMarketType.myket:
        return Myket();
      case AppMarketType.cafeBazaar:
        return Cafebazaar();
      case AppMarketType.playStore:
        return PlayStore();
      default:
        throw UnimplementedError();
    }
  }
}

enum AppMarketType {
  playStore,
  cafeBazaar,
  myket,
}
