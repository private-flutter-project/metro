import 'dart:io';

import 'package:android_intent_plus/android_intent.dart';
import 'package:flutter/foundation.dart';

import '../app_market.dart';
import '../common/app_market_mixin.dart';

class Myket with AppMarketMixin implements AppMarket {
  static Future<void> showAppComments({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'myket://comment?id=${packageName ?? await AppMarketMixin.getPackageName()}',
    );
    await intent.launch();
  }

  @override
  Future<void> showAppPage({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'myket://details?id=${packageName ?? await AppMarketMixin.getPackageName()}',
    );
    await intent.launch();
  }

  @override
  Future<void> showDevelopersApps({
    String? developerId,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'myket://developer/${developerId ?? AppMarketMixin.getPackageName()}',
    );
    await intent.launch();
  }

  static Future<void> showAppDownloadPage({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'myket://download/${packageName ?? AppMarketMixin.getPackageName()}',
    );
    await intent.launch();
  }
}
