import 'dart:io';

import 'package:android_intent_plus/android_intent.dart';
import 'package:flutter/foundation.dart';

import '../app_market.dart';
import '../common/app_market_mixin.dart';

class PlayStore with AppMarketMixin implements AppMarket {
  static const package = 'com.android.vending';

  @override
  Future<void> showAppPage({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'https://play.google.com/store/apps/details?id=${packageName ?? await AppMarketMixin.getPackageName()}',
      package: package,
    );
    await intent.launch();
  }

  @override
  Future<void> showDevelopersApps({
    String? developerId,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data: 'http://play.google.com/store/apps/dev?id=$developerId',
      package: package,
    );
    await intent.launch();
  }

  static Future<void> showAllApps({
    required String brand,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data: 'https://play.google.com/store/search?q=pub:$brand',
      package: package,
    );
    await intent.launch();
  }
}
