import 'dart:io';

import 'package:android_intent_plus/android_intent.dart';
import 'package:flutter/foundation.dart';

import '../app_market.dart';
import '../common/app_market_mixin.dart';

class Cafebazaar with AppMarketMixin implements AppMarket {
  static const package = 'com.farsitel.bazaar';

  static Future<void> showAppComments({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'android.intent.action.EDIT',
      data:
          'bazaar://details?id=${packageName ?? await AppMarketMixin.getPackageName()}',
      package: package,
    );
    await intent.launch();
  }

  @override
  Future<void> showAppPage({
    String? packageName,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data:
          'bazaar://details?id=${packageName ?? await AppMarketMixin.getPackageName()}',
      package: package,
    );
    await intent.launch();
  }

  @override
  Future<void> showDevelopersApps({
    String? developerId,
  }) async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = AndroidIntent(
      action: 'action_view',
      data: 'bazaar://collection?slug=by_author&aid=$developerId',
      package: package,
    );
    await intent.launch();
  }

  Future<void> loginPage() async {
    if (kIsWeb) return;
    if (!Platform.isAndroid) return;

    AndroidIntent intent = const AndroidIntent(
      action: 'action_view',
      data: 'bazaar://login',
      package: package,
    );
    await intent.launch();
  }
}
