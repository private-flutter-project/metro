import 'package:package_info_plus/package_info_plus.dart';

mixin AppMarketMixin {
  static Future<String> getPackageName() async {
    return (await PackageInfo.fromPlatform()).packageName;
  }
}
