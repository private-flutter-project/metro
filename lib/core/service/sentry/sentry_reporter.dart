import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import '../../const/const.dart';

class SentryReporter {
  late Sentry sentry;

  static Future<void> setup(Widget child) async {
    if (kDebugMode || Const.isDebugMode || kIsWeb) {
      runApp(child);
    } else {
      await SentryFlutter.init(
        (options) {
          options.dsn =
              'https://b9fe05b7cd5e4b60b28941045b29eb4a@sentry.hamravesh.com/5558';
          options.tracesSampleRate = 1.0;
          options.reportPackages = false;
          options.addInAppInclude('sentry_flutter_example');
          options.considerInAppFramesByDefault = false;
          options.attachThreads = true;
          options.enableWindowMetricBreadcrumbs = true;
          options.sendDefaultPii = true;
          options.reportSilentFlutterErrors = true;
          options.attachScreenshot = true;
          options.screenshotQuality = SentryScreenshotQuality.low;
          options.attachViewHierarchy = true;
          // options.environmentAttributes = {
          //   'app_version': '1.0.0',
          //   'device_name': 'My device',
          //   'device_id': '12345',
          // };
        },
        appRunner: () => runApp(
          SentryScreenshotWidget(
            child: SentryUserInteractionWidget(
              child: DefaultAssetBundle(
                bundle: SentryAssetBundle(
                  enableStructuredDataTracing: true,
                ),
                child: child,
              ),
            ),
          ),
        ),
      );
    }
  }

  static genericThrow(String message, Exception exc, {stackTrace}) async {
    if (stackTrace != null) {
      Sentry.captureException(exc, stackTrace: stackTrace);
    } else {
      Sentry.captureException(exc);
    }
  }

  static setupPerformance() async {
    final transaction = Sentry.startTransaction('processOrderBatch', 'task');
    try {
      await processOrderBatch(transaction);
    } catch (exc) {
      transaction.throwable = exc;
      transaction.status = const SpanStatus.internalError();
    } finally {
      await transaction.finish();
    }
  }

  static Future<void> processOrderBatch(ISentrySpan span) async {
    final innerSpan = span.startChild('task', description: 'operation');
    try {
      await Future.delayed(const Duration(seconds: 1));
    } catch (exc) {
      innerSpan.throwable = exc;
      innerSpan.status = const SpanStatus.notFound();
    } finally {
      await innerSpan.finish();
    }
  }
}
