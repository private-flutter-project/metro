import 'package:flutter/material.dart';
import 'package:metro/feature/Routing/routing_screen.dart';
import 'package:metro/feature/about_us/about_us.dart';
import 'package:metro/feature/map/map_screen.dart';
import 'package:metro/feature/settings/settings_screen.dart';
import 'package:metro/feature/stations/stations_screen.dart';

import '../../feature/splash/splash_screen.dart';
import '../../feature/stations_in_map/stations_in_map_screen.dart';

class Routes {
  static final Routes _instance = Routes._internal();

  factory Routes() => _instance;

  Routes._internal();

  static const String splash = "/splash";
  static const String map = "/map";
  static const String stations = "/stations";
  static const String routing = "/routing";
  static const String stationsInMap = "/stationsInMap";
  static const String settings = "/settings";
  static const String aboutUs = "/aboutUs";

  Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case splash:
        return screenRouting(const SplashScreen());

      case map:
        return screenRouting(const MapScreen());

      case stations:
        return screenRouting(const StationsScreen());

      case routing:
        return screenRouting(const RoutingScreen());

      case stationsInMap:
        return screenRouting(const StationsInMapScreen());

      case settings:
        return screenRouting(const SettingsScreen());

      case aboutUs:
        return screenRouting(const AboutUsScreen());

      default:
        return screenRouting(
          const Scaffold(
            body: Center(
              child: Text('تست'),
            ),
          ),
        );
    }
  }

  MaterialPageRoute screenRouting(Widget screen) {
    return MaterialPageRoute(
      builder: (context) => screen,
    );
  }
}
